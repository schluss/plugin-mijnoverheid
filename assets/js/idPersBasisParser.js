function runParser() {

    let r = {};

    // Personal details:

    // FirstNames
    r.FirstNames = getAttribute('#lo-auxiliary-list-005 > div:nth-child(1) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="voornamen"] p');

    // Surname
    r.Surname = getAttribute('#lo-auxiliary-list-005 > div:nth-child(2) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="geslachtsnaam"] p');

    // NameUsage
    r.NameUsage = getAttribute('#lo-auxiliary-list-005 > div:nth-child(3) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="naamgebruik"] p');

    // Gender
    r.Gender = getAttribute('#lo-auxiliary-list-005 > div:nth-child(4) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="geslacht"] p');

    // DateOfBirth
    r.DateOfBirth = getAttribute('#lo-auxiliary-list-005 > div:nth-child(6) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="geboortedatum"] p');

    // PlaceOfBirth
    r.PlaceOfBirth = getAttribute('#lo-auxiliary-list-005 > div:nth-child(7) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="geboorteplaats"] p');

    // CountryOfBirth
    r.CountryOfBirth = getAttribute('#lo-auxiliary-list-005 > div:nth-child(8) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="geboorteland"] p');

    // DocumentMunicipality
    r.DocumentMunicipality = getAttribute('#lo-auxiliary-list-005 > div:nth-child(9) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="gemeente document"] p');

    // DocumentDate
    r.DocumentDate = getAttribute('#lo-auxiliary-list-005 > div:nth-child(10) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="datum document"] p');

    // DocumentDescription
    r.DocumentDescription = getAttribute('#lo-auxiliary-list-005 > div:nth-child(11) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="beschrijving document"] p');

    // DocumentStartingDateValidity
    r.DocumentStartingDateValidity = getAttribute('#lo-auxiliary-list-005 > div:nth-child(12) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="ingangsdatum geldigheid"] p');

    // Address:

    return r;
}

// Run this function, the return value is catched in the plugin
runParser();

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
