function runParser() {

    let r = {};

    // Nationality
    r.Nationality = getAttribute('#nationaliteit-003-content > div > div:nth-child(1) > div.lo-auxiliary-toggle__header > span.lo-auxiliary-toggle__header-prefix > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p')
        || getAttribute('div[data-e2e-id="nationaliteit"] p'); // this is unverified!

    return r;
}

// Run this function, the return value is catched in the plugin
runParser();

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
