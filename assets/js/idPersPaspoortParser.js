function runParser() {

    let r = {};

    // IdType
    r.IdType = getAttribute('#reisdocument-005-content > div > div:nth-child(1) > div.lo-auxiliary-toggle__header > span > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p');

    // IdDocumentNumber
    r.IdDocumentNumber = getAttribute('#reisdocument-005-content > div > div:nth-child(2) > div.lo-auxiliary-toggle__header > span > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p');

    // IdDocumentExpiryDate
    r.IdDocumentExpiryDate = getAttribute('#reisdocument-005-content > div > div:nth-child(3) > div.lo-auxiliary-toggle__header > span > div > div > div.d-block.flex-sm-grow-1.lo-datalist__value > div > p');

    return r;
}

// Run this function, the return value is catched in the plugin
runParser();

function getAttribute(selector, attribute, dom) {
    if (attribute == null) attribute = 'innerText';
    if (dom == null) dom = document;
    let el = dom.querySelector(selector);
    if (el == null) return '';

    return dom.querySelector(selector)[attribute].trim();
}
