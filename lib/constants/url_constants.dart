class UrlConstants {
  static const int DATA_REQUEST_TOKEN_INDEX = 3; //Token position after spilitting "/"
  static const int DATA_REQUEST_CONFIG_URL_INDEX = 4; //Settings Url position after spilitting "/"
  static const int DATA_REQUEST_SCOPE_INDEX = 5; // Request Scope position after spilitting "/"
}
