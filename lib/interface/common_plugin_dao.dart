/*
 * @author Asanka Anthony
 * @email aanthony@yukon.lk
 * @create date 2020-11-12 09:16:23
 * @modify date 2020-11-12 09:16:23
 * Abstract class of all plugin
 */

abstract class CommonPluginDAO {
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'MijnOverheid',
  });

  Future<Map<String, dynamic>> getExtractedData(String filePath);
}
