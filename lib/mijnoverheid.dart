library mijnoverheid;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mijnoverheid/interface/common_plugin_dao.dart';
import 'package:mijnoverheid/ui/web_view_loading.dart';

class PluginMijnOverheid implements CommonPluginDAO {
  final StreamController<int> _streamController = StreamController.broadcast();
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void runPlugin(
    context,
    Function callBack,
    String siteUrl,
    String trigger, {
    String pluginName = 'MijnOverheid',
    Function(String userName, String password)? storeCredentialCallback,
    String? userName,
    String? password,
  }) {
    Locale locale;
    locale = Localizations.localeOf(context);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => WebViewLoading(
          callBack,
          siteUrl,
          trigger,
          locale,
          pluginName: pluginName,
          storeCredentialCallback: storeCredentialCallback,
          userName: userName,
          password: password,
        ),
      ),
    );
  }

  /// Gets extracted data from xml file.
  @override
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(40);
    Map<String, dynamic>? attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream; // Returns stream controller to the main app.
  }
}
