import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:mijnoverheid/constants/ui_constants.dart';
import 'package:mijnoverheid/model/translation_model.dart';
import 'package:mijnoverheid/reused_widgets/auth_warning_list.dart';
import 'package:mijnoverheid/reused_widgets/main_button.dart';
import 'package:mijnoverheid/reused_widgets/mini_header_template.dart';
import 'package:url_launcher/url_launcher.dart';

late BuildContext ctx;

class AuthSMSWarning extends StatelessWidget {
  final Locale locale;

  AuthSMSWarning(this.locale);

  @override
  Widget build(BuildContext context) {
    ctx = context;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var topPadding = MediaQuery.of(context).padding.top;

    TranslationModel.getTranslation(locale)!.loadingText;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: topPadding + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: Padding(
                  padding: EdgeInsets.only(
                    left: width * 0.07,
                    right: width * 0.07,
                    bottom: 0,
                  ),
                  child: ListView(children: [
                    SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/digid_and_sms.png',
                          package: 'mijnoverheid',
                          width: width * 0.4,
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Text(
                      'Oeps! Je sms-controle staat uit',
                      style: TextStyle(
                        color: UIConstants.headerTitleText,
                        fontSize: width * 0.05,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Dat heb je nodig voor inloggen met DigiD bij UWV.',
                          style: TextStyle(
                            color: UIConstants.mediumGrey,
                            fontSize: width * 0.05,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    AuhtWarningList([
                      AuhtWarningListItem(
                        '1',
                        'Ga naar',
                        child: RichText(
                          text: TextSpan(
                            text: '',
                            style: TextStyle(
                              color: UIConstants.mediumGrey,
                              fontSize: width * 0.05,
                            ),
                            children: [
                              TextSpan(
                                text: 'www.digid.nl',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () async {
                                    const url = 'https://www.digid.nl';
                                    if (await canLaunch(url)) {
                                      await launch(
                                        url,
                                        forceWebView: false,
                                        forceSafariVC: false,
                                      );
                                    }
                                  },
                              ),
                            ],
                          ),
                        ),
                      ),
                      AuhtWarningListItem('2', 'Klik op Mijn DigiD'),
                      AuhtWarningListItem('3', 'Log in'),
                      AuhtWarningListItem(
                        '4',
                        'Ga naar \'sms-controle\' en klik op \'sms-controle activeren\'',
                      ),
                    ]),
                    SizedBox(height: 20),
                    Padding(
                      padding: EdgeInsets.only(
                        right: width * 0.075,
                        left: width * 0.075,
                        bottom: 0,
                      ),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: MainButton(
                          'Opnieuw inloggen bij UWV',
                          () => Navigator.pop(ctx),
                        ),
                      ),
                    )
                  ])),
            ),
          ),
          // header
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(top: topPadding),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  locale: locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
