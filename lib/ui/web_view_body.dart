import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:logger/logger.dart';
import 'package:mijnoverheid/model/translation_model.dart';
import 'package:mijnoverheid/reused_widgets/wait_overlay.dart';
import 'package:mijnoverheid/ui/auth_sms_warning.dart';
import 'package:mijnoverheid/util/util.dart';

Logger _logger = Logger();
Locale? _local;
late BuildContext ctx;
bool isShown = false;
late bool _isTestEnvironment;
Map<String, dynamic> attributes = <String, dynamic>{};

class WebViewBody extends StatefulWidget {
  final Function webViewCreatedCallback; // use to call after webview created
  final String initialUrl; // initial url to open
  final Function urlChangedCallback; // use to call after webview created
  final Function mainAppCallBack; // main application callback function
  final Locale locale;
  final String pluginName;

  WebViewBody(
    this.webViewCreatedCallback,
    this.initialUrl,
    this.urlChangedCallback,
    this.mainAppCallBack,
    this.locale, {
    Key? key,
    this.pluginName = 'MijnOverheid',
  }) {
    _local = locale;

    // to keep track of whether we run this plugin on the test or real UWV environment
    _isTestEnvironment = !initialUrl.contains('mijn.overheid.nl');
  }

  @override
  State<WebViewBody> createState() => _WebViewBodyState();
}

class _WebViewBodyState extends State<WebViewBody> {
  @override
  void initState() {
    super.initState();
  }

  void showWaiter(String? text) async {
    if (!isShown) {
      WaitOverlay.show(ctx, _local, text: text);
      isShown = true;
    }
  }

  void hideWaiter() {
    if (isShown) {
      WaitOverlay.hide();
      isShown = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    ctx = context;

    // to make sure we do not end up in a loop during DigiD login
    var isFirst = true;

    @override
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;

    return Container(
      height: height * 0.9,
      width: width,
      child: Column(
        children: <Widget>[
          Expanded(
            child: InAppWebView(
                initialUrlRequest: URLRequest(url: Uri.parse(widget.initialUrl)),
                initialOptions: InAppWebViewGroupOptions(
                    android: AndroidInAppWebViewOptions(useHybridComposition: true),
                    crossPlatform: InAppWebViewOptions(
                        cacheEnabled: false, // Changes to true or false to enable or disable cache.
                        clearCache: true, // Clears all the webview's cache.
                        userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36')),
                onWebViewCreated: (InAppWebViewController appWebViewController) async {
                  // make sure attributes are empty
                  attributes = <String, dynamic>{};

                  showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);
                  widget.webViewCreatedCallback(appWebViewController); //call given function after webview created.
                },
                onLoadStart: (controller, url) async {
                  showWaiter(TranslationModel.getTranslation(widget.locale)!.processingText);
                },
                onLoadStop: (InAppWebViewController controller, url) async {
                  var location = Util.buildUrl(url.toString());

/* TODO 


na inloggen digid, indien je land op: 
https://mijn.overheid.nl/berichtenbox/nieuwe-organisaties/
klik mbhv javascript op opslaan:
#accept-afnemers
-> nog te onderzoeken of je direct kan navigeren naar de andere pages:


welkom page:
https://mijn.overheid.nl/welkom/

---

Identiteit:
https://mijn.overheid.nl/identiteit/

identiteit.persoonsgegevens.basis page:
https://mijn.overheid.nl/identiteit/persoonsgegevens/basis

identiteit.persoonsgegevens.familie page:
https://mijn.overheid.nl/identiteit/persoonsgegevens/familie

identiteit.persoonsgegevens.nationaliteit page:
https://mijn.overheid.nl/identiteit/persoonsgegevens/nationaliteit

identiteit.persoonsgegevens.paspoort-id-kaart page:
https://mijn.overheid.nl/identiteit/persoonsgegevens/paspoort-id-kaart

---

Financieen:
https://mijn.overheid.nl/financieel/

Mijn geregistreerd inkomen
https://mijn.overheid.nl/financieel/inkomen/

---

Wonen:
https://mijn.overheid.nl/wonen/

Kadaster page:
https://mijn.overheid.nl/wonen/kadaster/

WOZ gegevens page:
https://mijn.overheid.nl/wonen/woz/

WOZ details  page:
https://mijn.overheid.nl/wonen/woz/details/[150/015040010076]

---

Vervoer:
https://mijn.overheid.nl/vervoer/

Mijn Voertuiggegevens - Voertuig en APK page:
https://mijn.overheid.nl/vervoer/rdw/[12-AB-CD]/voertuig-apk

Mijn Voertuiggegevens - Eigenschappen page:
https://mijn.overheid.nl/vervoer/rdw/[12-AB-CD]/eigenschappen

Mijn Voertuiggegevens - Verbruik en milieu page:
https://mijn.overheid.nl/vervoer/rdw/[12-AB-CD]/verbruik-milieu

Mijn Voertuiggegevens - Historie page:
https://mijn.overheid.nl/vervoer/rdw/[12-AB-CD]/historie

---

Onderwijs:
https://mijn.overheid.nl/onderwijs/


andere categorieen:
- Werk
- Gezondheid
- Onderwijs

*/
                  _logger.i('loaded url: ' + location);

                  // TEST ENVIRONMENT
                  if (_isTestEnvironment) {
                    // 1. login page
                    if (location == Util.buildUrl(widget.initialUrl)) {
                      hideWaiter();
                    }

                    // 2. SMS auth page:
                    if (location.contains('digid-sms.html')) {
                      hideWaiter();
                    }

                    // after logging in, do the magic:

                    // 3. welkom page
                    if (location.contains('welkom.html')) {
                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(Util.buildUrl(widget.initialUrl, path: 'id.pers.basis.html'))));
                    }

                    // 4. Identiteit -> persoonsgegevens basis page
                    if (location.contains('id.pers.basis.html')) {
                      await parsePage(controller, 'idPersBasisParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(Util.buildUrl(widget.initialUrl, path: 'id.pers.familie.html'))));
                    }

                    // 5. Identiteit -> persoonsgegevens familie page
                    if (location.contains('id.pers.familie.html')) {
                      //await parsePage(controller, 'identiteitPersoonsgegevensParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(Util.buildUrl(widget.initialUrl, path: 'id.pers.nationaliteit.html'))));
                    }

                    // 6. Identiteit -> persoonsgegevens nationaliteit page
                    if (location.contains('id.pers.nationaliteit.html')) {
                      await parsePage(controller, 'idPersNationaliteitParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(Util.buildUrl(widget.initialUrl, path: 'id.pers.paspoort-id-kaart.html'))));
                    }

                    // 7. Identiteit -> persoonsgegevens paspoort-id-kaart page
                    if (location.contains('id.pers.paspoort-id-kaart.html')) {
                      await parsePage(controller, 'idPersPaspoortParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(Util.buildUrl(widget.initialUrl, path: 'aktevanoverlijden.html'))));
                    }

                    // 8. Akte van Overlijden page
                    if (location.contains('aktevanoverlijden.html')) {
                      await parsePage(controller, 'akteVanOverlijdenParser.js');

                      hideWaiter();

                      // done, return
                      widget.mainAppCallBack(jsonEncode(attributes), context);
                    }
                  }
                  // LIVE ENVIRONMENT
                  else {
                    if (location == 'https://mijn.overheid.nl/' || location == 'https://mijn.overheid.nl') {
                      // programatically click on the digid login link
                      var js = ''' 

                        var schlussLink = document.querySelector('#navigation-main > form > a');
                        if (schlussLink){
                          schlussLink.click();
                        }
                        ''';

                      await controller.evaluateJavascript(source: js);
                    }

                    // when on the digid main login page
                    // we get here when:
                    // a: entering login landing page: redirect to login with username & password page (=inloggen_sms)
                    // b: post after login: do not do anything here (isfirst is already set, which prevents an endless loop)
                    // c: login failed because sms verification is disabled: show AuthSMSWarning
                    if (location == 'https://digid.nl/inloggen' || location == 'https://digid.nl/inloggen/') {
                      // check if sms auth is enabled, if not: show error to the user
                      bool smsAuthDisabled = await (controller.evaluateJavascript(source: '''
                      (function(){
                        let el = document.querySelector("#main_content > div.block-with-icon--error > p");
                        if (el) {
                          console.log('innerText sms check is: ' + el.innerText);
                          return el.innerText.includes('sms');
                        }
                        return false;
                      })();'''));

                      if (smsAuthDisabled) {
                        hideWaiter();
                        Navigator.pop(context);
                        await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AuthSMSWarning(widget.locale)),
                        );
                        return;
                      }

                      if (isFirst) {
                        isFirst = false;
                        await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://digid.nl/inloggen_sms')));
                      }
                    }

                    // when at digid login / auth pages: hide waiter to allow user
                    if (location == 'https://digid.nl/inloggen_sms' || location == 'https://digid.nl/sms_controleren' || location == 'https://digid.nl/bevestig_telefoonnummer') {
                      hideWaiter();
                    }

                    // after login, could be you land on a page where you need to supply consent for newly connected organizations
                    // if so, just go on to the welkom page
                    if (location.contains('mijn.overheid.nl/berichtenbox/nieuwe-organisaties')) {
                      var js = ''' 
                        window.setTimeout(function(){ 
                          var orgLink = document.getElementById('#accept-afnemers');
                          if (orgLink){
                            orgLink.click();
                          }
                          
                          // second, direct attempt to submit the form - just to be sure!
                          document.forms[0].submit();

                        }, 1000);
                        ''';

                      await controller.evaluateJavascript(source: js);

                      //await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.overheid.nl/welkom')));
                    }

                    // 3. welkom page
                    if (location.contains('mijn.overheid.nl/welkom')) {
                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.overheid.nl/identiteit/persoonsgegevens/basis')));
                    }

                    // 4. Identiteit -> persoonsgegevens basis page
                    if (location.contains('mijn.overheid.nl/identiteit/persoonsgegevens/basis')) {
                      await parsePage(controller, 'idPersBasisParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.overheid.nl/identiteit/persoonsgegevens/familie')));
                    }

                    // 5. Identiteit -> persoonsgegevens familie page
                    if (location.contains('mijn.overheid.nl/identiteit/persoonsgegevens/familie')) {
                      //await parsePage(controller, 'identiteitPersoonsgegevensParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.overheid.nl/identiteit/persoonsgegevens/nationaliteit')));
                    }

                    // 6. Identiteit -> persoonsgegevens nationaliteit page
                    if (location.contains('mijn.overheid.nl/identiteit/persoonsgegevens/nationaliteit')) {
                      await parsePage(controller, 'idPersNationaliteitParser.js');

                      // go to the next page
                      await controller.loadUrl(urlRequest: URLRequest(url: Uri.parse('https://mijn.overheid.nl/identiteit/persoonsgegevens/paspoort-id-kaart')));
                    }

                    // 7. Identiteit -> persoonsgegevens paspoort-id-kaart page
                    if (location.contains('mijn.overheid.nl/identiteit/persoonsgegevens/paspoort-id-kaart')) {
                      await parsePage(controller, 'idPersPaspoortParser.js');

                      hideWaiter();

                      // done, return
                      widget.mainAppCallBack(jsonEncode(attributes), context);
                    }
                  }
                }),
          )
        ],
      ),
    );
  }

  void parseIdentiteit() {}

  Future parsePage(InAppWebViewController controller, String parserFilename) async {
    // parse data
    var parsedData = await controller.evaluateJavascript(
      source: await DefaultAssetBundle.of(ctx).loadString('packages/mijnoverheid/assets/js/' + parserFilename),
    );

    if (parsedData == null) {
      return;
    }

    var tmpMap = Map<String, dynamic>.from(parsedData); // Explicit Cast to <String, dynamic>, otherwise IOS error

    // add to attributes global
    attributes.addAll(tmpMap);
  }
}
