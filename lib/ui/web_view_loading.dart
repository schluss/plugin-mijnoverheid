import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:mijnoverheid/constants/ui_constants.dart';
import 'package:mijnoverheid/model/translation_model.dart';
import 'package:mijnoverheid/reused_widgets/loading_indicator.dart';
import 'package:mijnoverheid/reused_widgets/mini_header_template.dart';
import 'package:mijnoverheid/ui/web_view_body.dart';

class WebViewLoading extends StatefulWidget {
  final Function callBack; // function to callback main application
  final String siteUrl;
  final String trigger;
  final String? pluginName;
  final Locale locale;
  final Function(String userName, String password)? storeCredentialCallback;
  final String? userName;
  final String? password;

  WebViewLoading(
    this.callBack,
    this.siteUrl,
    this.trigger,
    this.locale, {
    this.pluginName,
    this.storeCredentialCallback,
    this.userName,
    this.password,
  });

  @override
  _WebViewLoading createState() => _WebViewLoading();
}

class _WebViewLoading extends State<WebViewLoading> {
  var _url;
  bool _isLoading = true;

  /// Change this variable to show waiting text.
  final bool _isWaiting = false;

  late Timer _timer;
  _WebViewLoading() {
    /// Set timer but not work with inappbrowser widget.
    _timer = Timer(const Duration(seconds: 3), () {
      setState(() {
        _isLoading = false;
      });
    });
  }

  InAppWebViewController? _appWebViewController; // Contains webview controller object.

  void _afterInAppWebCreated(InAppWebViewController webViewController) {
    setState(() {
      _appWebViewController = webViewController;
    });
  }

  void _urlChanged(url) {
    setState(() {
      _url = url; // set url change
    });
  }

  /// Callbacks to main application.
  void _callBackMethod(String data, BuildContext context) {
    widget.callBack(widget.pluginName, data, context); //callback to main application
  }

  @override
  Widget build(BuildContext context) {
    _url = widget.siteUrl;
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var topPadding = MediaQuery.of(context).padding.top;

    var loadingText = TranslationModel.getTranslation(widget.locale)!.loadingText;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              top: topPadding + height * 0.1,
            ),
            height: height,
            width: width,
            color: UIConstants.primaryColor,
            child: Container(
              height: height * 0.9,
              width: width,
              child: !_isLoading && !_isWaiting
                  ? WebViewBody(
                      _afterInAppWebCreated,
                      _url,
                      _urlChanged,
                      _callBackMethod,
                      widget.locale,
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                        left: width * 0.07,
                        right: width * 0.07,
                        bottom: height * 0.25,
                      ),
                      child: LoadingIndicator(
                        widget.locale,
                        text: _isLoading ? loadingText : TranslationModel.getTranslation(widget.locale)!.processingText,
                      ),
                    ),
            ),
          ),
          Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: height * 0.1,
                    color: UIConstants.paleLilac,
                  )),
              Padding(
                padding: EdgeInsets.only(top: topPadding),
                child: MiniHeaderTemplate(
                  iconPath: 'assets/images/lock_icon.png',
                  title: _url,
                  locale: widget.locale,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }
}
