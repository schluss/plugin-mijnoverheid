import 'dart:io';

import 'package:fluri/fluri.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart' as in_app;
import 'package:logger/logger.dart';
import 'package:path_provider/path_provider.dart';
import 'package:requests/requests.dart';

Logger _logger = Logger();

class Util {
  static final String _fileName = 'verzekeringsbericht.pdf';

  /// Builds a valid url from a given endpoint and optional path.
  /// For example endpoint: [https://site.com/] and path: [/path/to/object],
  /// resolve into  [https://site.com/path/to/object].
  static String buildUrl(String endpoint, {String? path}) {
    var uri = Fluri(endpoint);

    if (path != null) {
      uri.addPathSegment(path);
    }

    var url = uri.toString();

    // remove trailing slash to make the outputted url always uniform
    if (url.endsWith('/')) {
      url = url.substring(0, url.length - 1);
    }

    return url;
  }

  // Download and store the PDF file
  static Future<String> downloadAndSaveFile(
    String downloadUrl,
    List<in_app.Cookie> cookies,
  ) async {
    // prepare and format cookies
    Map<String, String?> cookiesMap;
    cookiesMap = {};

    cookies.forEach((element) {
      if (element != null) {
        cookiesMap[element.name] = element.value;
      }
    });

    // Prepare request
    String hostname;
    hostname = Requests.getHostname(downloadUrl);
    await Requests.setStoredCookies(hostname, cookiesMap as Map<String, String>);

    // Do the request and get the response
    var response = await Requests.get(downloadUrl);

    _logger.d('UWV plugin - response statuscode: ' + response.statusCode.toString());

    List<int> body;
    body = response.bytes();

    // Store result as a file
    String savePath;
    savePath = await getSavePath(_fileName); // Getting file saving location.

    File f;
    f = File(await getSavePath(_fileName));
    await f.writeAsBytes(body);

    _logger.d('UWV plugin - written to: ' + savePath);

    return savePath;
  }

  // Make the file saving location
  static Future<String> getSavePath(String fileName) async {
    var _dir = await getApplicationDocumentsDirectory();
    return '${_dir.path}/$fileName';
  }
}
